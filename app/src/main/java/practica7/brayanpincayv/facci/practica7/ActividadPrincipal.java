package practica7.brayanpincayv.facci.practica7;

import android.content.Context;
import android.os.Vibrator;
import android.service.notification.Condition;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadPrincipal extends AppCompatActivity {
    Button botonVibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);

        final Vibrator vibrator=(Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);

        botonVibrar=(Button)findViewById(R.id.btnVibrar);

        botonVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vibrator.vibrate(600);
            }
        });

    }
}
